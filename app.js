const express = require('express');
const bodyPrse = require('body-parser')
const app = express();
app.use(bodyPrse.urlencoded({extented:true}));
app.use(bodyPrse.json());
const port = 8000;

//habilitar servidor
app.listen(port,()=>{
    console.log("projeto executando na porta:" + port);
}); //CTRL+C para parar 

app.get('/aluno',(req,res)=>{
    res.send("{mesage:Aluno encontrado!}");
});
//recurso de request.query
app.get('/aluno/filtros',(req,res)=>{
    let source = req.query;
    let ret = "Dados solicitados: " + source.nome + " " + source.sobrenome;
    res.send("{message: "+ret+" }");
});
//recurso de request.param
app.get('/aluno/pesquisa/:valor',(req,res)=>{
    console.log("entrou")
    let dado = req.params.valor;
    let ret = "Dados solicitados: " + dado;
    res.send("{message: "+ret+" }");
});

//recursode request.body
app.post('/aluno',(req,res)=>{
    let dados = req.body;
    let ret = "\nDados enviados\nNome:" + dados.nome;
    ret+="\nSobrenome:" + dados.sobrenome;
    ret+="\nIdade:" + dados.idade;
    res.send("{Message:" + ret + "}");
});